#! /bin/python3

"""
This script processes the given c source files and looks for unneeded
#include statements.

It was created to work on any source code regardless of
the C compiler. My embedded C code would not compile for use by the several
Clang-based utilities that perform similar functions. No claims are made to
the accuracy of this script, but in general it has worked well for me.

The script looks through all source files provided and extracts their exclude
commands. From their the files referenced by the #include statements are
parsed by ctags looking for all elements (defines, variables, enums and
their elements, structs and their sub-elements. The extracted elements are
searched for in the c source files. If no element from a given #include is
found in that file, it is assumed that the #include is not needed.

It should be noted that this script will not expand #include statements found
within the #include statements in the given c files.

usage: unneeded_include_finder.py [-h] [--include INCLUDE] [--ignore IGNORE]
                                  [--ctags_exec CTAGS_EXEC]
                                  files_to_check [files_to_check ...]

Process the given files to see if its included files are necessary.

positional arguments:
  files_to_check        Files to check if their includes are needed

optional arguments:
  -h, --help            show this help message and exit
  --include INCLUDE, -i INCLUDE
                        Add an additional path where an included file may be
                        found. Each additional path must be added separately
  --ignore IGNORE, -g IGNORE
                        Ignored files will not be checked for use. If a source
                        file has "#include "foo.h" and does not use any tag in
                        that file, but "foo.h" is ignored, it's use will not
                        be checked.
  --ctags_exec CTAGS_EXEC, -c CTAGS_EXEC
                        Path of the ctags executable to use

*******************************************************************************
MIT License

Copyright (c) 2020 Christopher Meyer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import sys
import re

valid_file_types = [".c", ".cpp", ".h"]
include_regex = re.compile(r'^[ \t]*#include[ \t]*\"(.*\.h)\"')


def err_print(*args, **kwargs):
    """
    Print the given string to stderr
    :param args: String to print on stderr
    :param kwargs:
    :return: None
    """
    print(*args, file=sys.stderr, **kwargs)


def check_all_source_files_for_unneeded_includes(files_path: (str, list),
                                                 files_to_exclude: (
                                                         list, None) = None,
                                                 include_paths: list = None,
                                                 print_message_for_all=False,
                                                 exclude_complementary=True,
                                                 remove_unneeded=False):
    """
    Processes the given file(s) looking for unneeded includes. Results
    will be printed to stdout
    :param files_path: Files to analyze. Can include wild card,
    directories, or list of files
    :param files_to_exclude: When included in the given source file, these
    files will not be checked to see if they are needed
    :param print_message_for_all: Print message if the file is needed.
    Normally a message will only be printed if the fill is not needed
    :param include_paths: Additional paths to search for include files if
    they are not found relative to the active file
    :param exclude_complementary: Ignore complementary header include even
    if they are not used, e.g. foo.c and foo.h
    :param remove_unneeded: Remove unneeded includes from the given files
    :return: None
    """

    if type(files_path) is str:
        # If a string is given, convert it to a list.
        # All work will be done on the lists
        files_path = [files_path]

    if files_path is not None:
        import os

        full_files_path = []
        for a_path in files_path:

            # Walk through each directory
            if os.path.isdir(a_path):
                for dir_path, dir_names, file_names in os.walk(a_path):
                    if not file_names:
                        # If there are no files in this directory, continue
                        continue

                    for a_file in file_names:
                        # Add all valid files to the list
                        if os.path.splitext(a_file)[1] in valid_file_types:
                            full_files_path.append(os.path.join(dir_path,
                                                                a_file))

            # Only add valid files to the list
            elif os.path.splitext(a_path)[1] in valid_file_types:
                full_files_path.append(a_path)

        # Check each file
        unneeded_count = 0;
        for a_path in full_files_path:
            unneeded_count += \
                check_a_source_file_for_unneeded_includes(
                    a_path,
                    files_to_exclude,
                    include_paths,
                    print_message_for_all,
                    exclude_complementary,
                    remove_unneeded)

        print("Complete: {} uneeded include statements".format(unneeded_count))


def check_a_source_file_for_unneeded_includes(source_file_path: str,
                                              files_to_exclude: (list,
                                                                 None) = None,
                                              include_paths: list = None,
                                              print_message_for_all=False,
                                              exclude_complementary=True,
                                              remove_unneeded=False) -> int:
    """
    Processes the given source file looking for unneeded includes. Results
    will be printed to stdout.

    If print_messages_for_all is False, only the file name and include
    will be printed. There will be no mention of whether or not it was needed.

    :param source_file_path: File to analyze
    :param files_to_exclude: When included in the given source file, these
    files will not be checked to see if they are needed
    :param include_paths: Additional paths to search for include files if
    they are not found relative to the active file
    :param print_message_for_all: Print message if the file is needed.
    :param exclude_complementary: Ignore complementary header include even
    if they are not used, e.g. foo.c and foo.h
    Normally a message will only be printed if the fill is not needed
    :param remove_unneeded: Remove includes found to be not needed
    :return: Number of unneeded includes
    """

    import os

    unneeded_includes = []

    if os.path.isfile(source_file_path):

        # Get a list of all of the includes (excluding those to ignore)
        # then get their tags
        list_of_includes_with_lines = get_included_files_from_source_file(
            source_file_path,
            include_paths)

        # Create a list of include path from tuple list
        list_of_includes = [item[0] for item in list_of_includes_with_lines]

        # Add the complementary header if it is to be excluded
        if exclude_complementary:
            if type(files_to_exclude) is not list:
                files_to_exclude = []

            import os
            complementary_header = os.path.splitext(source_file_path)[0] + '.h'
            files_to_exclude.append(complementary_header)

        # Strip out the ignored includes from the path-only list
        list_of_includes = remove_files_from_list(list_of_includes,
                                                  files_to_exclude)

        # Update the path and line list to match the path-only list
        path_list_index = 0
        temp_path_with_line_list = []

        if len(list_of_includes) > 0:
            for path_with_line in list_of_includes_with_lines:
                # If the next item in the list_of_includes is the current
                # path in the list of paths with lines add it on
                if list_of_includes[path_list_index] == path_with_line[0]:
                    temp_path_with_line_list.append(path_with_line)

                    path_list_index += 1

                    # Once all includes have been non-ignored found, stop
                    # Otherwise invalid index errors will get thrown
                    if path_list_index >= len(list_of_includes):
                        break

        list_of_includes_with_lines = temp_path_with_line_list

        # Generate the ctags data on the non-ignored include files
        ctags_file_path = get_ctags_file_for_files(list_of_includes)

        # Initialize counters
        current_include_index = 0

        # Check each included file to see if they are required
        for an_include, line_number in list_of_includes_with_lines:
            needed = source_file_requires_include(source_file_path,
                                                  an_include,
                                                  ctags_file_path)

            # If None was returned, there was an error processing the source
            # file, the include, or ctags file. Let the user know
            if needed is None:
                err_print("Error checking " + an_include)

            elif (not needed) or print_message_for_all:
                # include was parsed successfully. Let the user know of
                # the result
                print("{}:{}:0:{} ".format(
                    source_file_path,
                    line_number,
                    os.path.relpath(an_include)), end="")

                if not needed:
                    unneeded_includes.append((an_include, line_number))

                if print_message_for_all:
                    if not needed:
                        print("NOT ", end="")
                    print("needed.")
                else:
                    print("")

            current_include_index += 1

        if remove_unneeded and (len(unneeded_includes) > 0):
            remove_unneeded_include_lines_from_source_file(
                source_file_path,
                unneeded_includes)

        # Delete the ctags output file
        os.unlink(ctags_file_path)

    else:
        if source_file_path is None:
            err_print("No source file was given")
        else:
            err_print(source_file_path + " is not a file.")

    return len(unneeded_includes)


def remove_unneeded_include_lines_from_source_file(source_file_path: str,
                                                   includes_to_remove: list):
    """
    Removes the given includes from the given source file.

    The function works by tracking the line numbers. If the line in the given
    list does not have that include, that include will not be removed.

    Only the include will be removed. If there is more than the include
    (excluding whitespace) on the line everythng else will be left alone

    Assumes only one include per line

    :param source_file_path: Path to the source file to edit
    :param includes_to_remove: List of includes and their line numbers
    to remove. Items are expected to be in line number order
    :return: None
    """
    import os

    # Don't create a file if it does not exist and don't remove anything
    # if there is nothing to remove
    if os.path.exists(source_file_path) and len(includes_to_remove) > 0:

        # Create a copy otherwise this function will modify the list
        includes_to_remove_local = includes_to_remove.copy()

        # Read all of the file
        with open(source_file_path, 'r') as source_file:
            read_lines = source_file.readlines()

        with open(source_file_path, 'w') as source_file:
            # Go through all of the just read lines looking for the
            # includes to remove

            current_line_number = 1

            # Get the path and line number of the first include to remove
            search_inc_path, search_inc_line_number = \
                includes_to_remove_local.pop(0)
            search_inc_basename = os.path.basename(search_inc_path)

            pop_new_include = False

            for a_line in read_lines:
                if ((current_line_number == search_inc_line_number) and \
                        (search_inc_basename in a_line)):

                    a_line = re.sub(include_regex, "", a_line, count=1)
                    a_line = a_line.strip()

                    pop_new_include = True

                    if len(a_line) > 0:
                        source_file.write(a_line)
                else:
                    source_file.write(a_line)

                current_line_number += 1

                # Find the next include to remove
                if pop_new_include or \
                        (current_line_number >= search_inc_line_number):
                    pop_new_include = False

                    while current_line_number > search_inc_line_number:

                        try:
                            # Get the path and line number of the next include
                            # to remove
                            search_inc_path, search_inc_line_number = \
                                includes_to_remove_local.pop(0)
                            search_inc_basename = os.path.basename(
                                search_inc_path)
                        except IndexError:
                            # No more items to remove. The rest of the file
                            # will need to be written, so we cannot quit
                            # outright
                            search_inc_line_number = len(read_lines) + 1


def get_included_files_from_source_file(source_file_path: str,
                                        include_paths: list = None,) -> (list, None):
    """
    Returns a list of header file includes from the given source file

    Only files that exist will be returned.

    Will not find any within "<>"

    :param source_file_path: Path of the source file to read
    :param include_paths: If included file can not be found relative to
    source file, these paths will be searched in order.
    :return: List of tuples if source file was valid. If it was not,
        None will be returned
        Each tuple will have the format
            Absolute paths to the included file
            Line in the given source file where it is referenced
    """
    included_files = None

    with open(source_file_path, 'r') as source_file:
        import os

        included_files = []

        source_file_abs_path = os.path.abspath(source_file_path)
        source_file_abs_dir_path = os.path.dirname(source_file_abs_path)

        curr_line_number = 1
        for line in source_file:
            # Find all lines with "#include "..."
            matches = re.findall(include_regex, line)

            if matches:
                for a_match in matches:
                    local_paths = [source_file_abs_dir_path]

                    # If additional paths were provided add them
                    if type(include_paths) is list:
                        local_paths.extend(include_paths)

                    # Go through all of the possible paths until the file
                    # is found
                    for a_local_path in local_paths:
                        match_abs_path = os.path.join(a_local_path,
                                                      a_match)
                        match_abs_path = os.path.abspath(match_abs_path)
                        if os.path.isfile(match_abs_path):
                            included_files.append((match_abs_path,
                                                   curr_line_number))
                            break

            curr_line_number += 1

    return included_files


def remove_files_from_list(haystack: list, to_remove: list) -> list:
    """
    Remove the list of files from the haystack
    :param haystack: List of absolute file paths from which files are to
    be removed from
    :param to_remove: List of files to remove
    :return: haystack minus the files in to_remove
    """
    out_list = haystack
    if list is not None and to_remove is not None:
        import os

        to_remove_norm = [os.path.normpath(path) for path in to_remove]

        out_list = [currFile for currFile in haystack
                    if not currFile.endswith(tuple(to_remove_norm))]

    return out_list


def source_file_requires_include(source_file_path: str,
                                 included_file_path: str,
                                 ctags_file: (str, None) = None
                                 ) -> (bool, None):
    """
    Check if the given source file requires the contents of the given
    include file

    :param source_file_path: Path of the source file to check
    :param included_file_path: Path of the include file to check
    :param ctags_file: ctags file containing the tags for the given include.
    If value is none, ctags will be run on included_file_path
    :return: True if the include is required by the source file,
    False if it isn't,
    None if there was an error reading either of the files
    """

    # If no ctags file was given, parse the given include
    if ctags_file is None:
        ctags_file = get_ctags_file_for_files([included_file_path])

    include_is_needed = None

    if ctags_file is not None:
        # Maybe there was a failure parsing the include, so None needs to be
        # checked again

        # Include file was parsed, now check if it is actually needed
        include_is_needed = tags_are_in_source_file(
            source_file_path=source_file_path,
            include_file_path=included_file_path,
            tags_result_file_path=ctags_file)

    return include_is_needed


def get_ctags_file_for_files(includes_to_parse: list) -> (str, None):
    """
    Runs the ctags command on the given file and if it was successfully parsed,
    the path the to file outputted by ctags is returned.

    :param includes_to_parse: Include file whose tags are to be read. Paths
    are assumed to be valid and will not be checked.
    :return: Path of the ctags output file if ctags was successful,
    None otherwise
    """
    if includes_to_parse is not None and len(includes_to_parse) > 0:
        import subprocess

        tags_result_file = "unneeded_include_finder_tags.txt"
        ctags_args = ["-f " + tags_result_file,
                      "--extras=-F",
                      "--if0=yes", "--c-kinds=defgmpstuvx",
                      "--fields=afmikKlnsStz"]
        run_args = [ctags_path]
        run_args.extend(ctags_args)
        run_args.extend(includes_to_parse)

        # Run ctags with the given params
        output = subprocess.run(run_args, capture_output=True)

        # There was an error running ctags, print it to stderr
        # Return None
        if (output.returncode != 0) or (len(output.stderr) > 0):
            err_print("Error processing include {}:\n{}".format(
                includes_to_parse,
                output.stderr.decode('utf-8')))
            tags_result_file = None

    else:
        tags_result_file = None

    return tags_result_file


def tags_are_in_source_file(source_file_path: str,
                            include_file_path: str,
                            tags_result_file_path: str
                            ) -> (bool, None):
    """
    Returns true if any of the tags in the given ctags result file are in
    the given source file

    Only one tag has to be found before this function will exit successfully

    :param source_file_path: Source file to search
    :param include_file_path: Include file to check if it is needed
    :param tags_result_file_path: ctags result file containing tags to look
    for in the source file
    :return: True if the tags are present. False will be returned if the
    source or results file cannot be opened
    """

    include_file_needed = None

    # Get a dict of files and tags
    tags = parse_ctags_output_file(tags_result_file_path)

    # Continue if the ctags file was parsed successfully and
    # the given include was in there
    if (tags is not None) and (include_file_path in tags):

        # Read all of the file's contents
        with open(source_file_path, 'r') as source_file:
            source_file_contents = source_file.read()

            # Remove comments from the file. If a reference to a variable is
            # commented out remove it does not count as being used
            source_file_contents = remove_comments_from_text(source_file_contents)

            # Check if at least one of the tags was in the source file
            include_file_needed = \
                any(a_tag in source_file_contents
                    for a_tag in tags[include_file_path])

    return include_file_needed

def remove_comments_from_text(in_text:str) -> str:
    """
    Remove comments from the given text
    :param in_text: Text from which comments are to be removed
    :return: in_text without the comments
    """
    # Remove // coments
    in_text = re.sub(r'.*//.*', '', in_text)

    # Remove /* */ comments
    multiline_regex = re.compile(r'/\*([^\*]*\*+[^\*/])*([^\*]*)\*+|[^\*]*\*/',
                                 re.MULTILINE | re.DOTALL)
    in_text = re.sub(multiline_regex, '', in_text)
    return in_text

def parse_ctags_output_file(ctags_output_file: str) -> (dict, None):
    """
    Parses the given ctags output file for tag_names and the files
    that they're from. Any other information in the file is ignored.

    :param ctags_output_file: ctags output file to parse
    :return: A dict with files as the keys and a list of tags as the value.
    None if there was a problem loading the tags file
    """

    read_tags = None
    with open(ctags_output_file, 'r') as f:
        import os

        read_tags = {}

        for line in f:
            # Ignore the ctags file header
            if not line.startswith("!_TAG_"):

                # All the we care about are the tag and the file that its from
                tag_name, file_name, trash = line.split("\t", maxsplit=2)

                # Make sure the slashes are standard for the OS
                # In windows, ctags outputs paths with '/'
                file_name = os.path.normpath(file_name)

                # If the file already has an key in the dict, append
                # to the list, otherwise add a new key
                if file_name in read_tags:
                    read_tags[file_name].append(tag_name)
                else:
                    read_tags[file_name] = [tag_name]

    return read_tags


def validate_ctags_executable(in_path:str) -> bool:
    """
    Checks to see if the given ctags executable exists
    :param in_path:
    :return:
    """
    import os
    import subprocess

    ctags_is_valid = True

    if not os.path.isfile(in_path):
        # The given path did not exist, but the executable may be on the
        # system path, so try to run it first
        try:
            subprocess.run(in_path, capture_output=True)
        except FileNotFoundError:
            # ctags could not be found, so script will fail
            ctags_is_valid = False

    return ctags_is_valid

if __name__ == "__main__":
    import os

    # Set up the argument parser
    import argparse
    parser = argparse.ArgumentParser(description="Process the given files to "
                                                 "see if its included files "
                                                 "are necessary.")
    parser.add_argument('--include', '-i',
                        action='append',
                        help='Add an additional path where an included file '
                             'may be found. Each additional path must be '
                             'added separately')
    parser.add_argument('--ignore', '-g',
                        action='append',
                        help='Ignored files will not be checked for use. If '
                             'a source file has "#include "foo.h" and does '
                             'not use any tag in that file, but "foo.h" is '
                             'ignored, it\'s use will not be checked.')

    parser.add_argument('--ctags_exec', '-c',
                        help='Path of the ctags executable to use')

    parser.add_argument('--write', '-w',
                        action='store_true',
                        default=False,
                        dest='rewrite_files',
                        help='Remove unnecessary includes from the '
                             'source file')

    parser.add_argument('--check_matching_header', '-m',
                        action='store_true',
                        default=False,
                        help='Check if a source file\'s complementary header '
                             'is unused. e.g. foo.c and foo.h. Defaults to '
                             'off/ignore.')

    parser.add_argument('files_to_check',
                        nargs='+',
                        help='Files to check if their includes are needed')

    args = parser.parse_args()

    # If a ctags exe was given update the variable referencing the ctags path
    if args.ctags_exec is not None:
        ctags_path = args.ctags_exec
    else:
        ctags_path = 'ctags'


    # Verify that the ctags file exists before continuing
    if validate_ctags_executable(ctags_path):

        # Set up the list for the extra include paths.
        if args.include is None:
            # If none was given just create an empty list
            extra_include_paths_abs = []
        else:
            # If extra include paths were given, make the paths absolute
            extra_include_paths_abs = [os.path.abspath(a_path)
                                       for a_path in args.include]

        check_all_source_files_for_unneeded_includes(
            files_path=args.files_to_check,
            files_to_exclude=args.ignore,
            include_paths=extra_include_paths_abs,
            exclude_complementary=(not args.check_matching_header),
            remove_unneeded=args.rewrite_files)
    else:
        err_print("ctags executable \"{}\" does not exist".format(ctags_path))
