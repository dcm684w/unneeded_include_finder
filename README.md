# Unneeded Include Finder
This script processes the given c source files and looks for unneeded
\#include statements.

## How it works
The script looks through all source files provided and extracts their exclude
commands. From their the files referenced by the #include statements are
parsed by ctags looking for all elements (defines, variables, enums and
their elements, structs and their sub-elements. The extracted elements are
searched for in the c source files. If no element from a given \#include is
found in that file, it is assumed that the #include is not needed.

It should be noted that this script will not expand #include statements found
within the #include statements in the given c files.

## Usage
`unneeded_include_finder.py [-h] [--include INCLUDE] [--ignore IGNORE]
                                [--ctags_exec CTAGS_EXEC]
                                files_to_check [files_to_check ...]`

Process the given files to see if its included files are necessary.

*positional arguments:*

  `files_to_check`        Files to check if their includes are needed

*optional arguments:*

  `-h`, `--help`            show this help message and exit
  
  `--include INCLUDE`, `-i INCLUDE`
                        Add an additional path where an included file may be
                        found. Each additional path must be added separately
                        
  `--ignore IGNORE`, `-g IGNORE`
                        Ignored files will not be checked for use. If a source
                        file has `#include "foo.h` and does not use any tag in
                        that file, but "foo.h" is ignored, it's use will not
                        be checked.
                        
  `--ctags_exec CTAGS_EXEC`, `-c CTAGS_EXEC`
                        Path of the ctags executable to use.
                        
  `--write`, `-w`           Remove unnecessary includes from the source file
  
  `--check_matching_header`, `-m`
                        Check if a source file's complementary header is
                        unused. e.g. foo.c and foo.h. Defaults to off/ignore.

## Requirements
- Python 3. Script was written on Python 3.8 and has not been tested on 
other versions.
- [Universal ctags](https://github.com/universal-ctags/ctags)

This script has been tested and been found to work on Windows and on Linux 

## Examples
Analyze "src/racecar.c" and do not check if "foo.h" or "bar.h" are needed.

`unneeded_include_finder.py -g "foo.h" -g "bar.h" "src/racecar.c"`

Analyze "tugboat.c" and check the folder "../../more_includes" if any 
included files cannot be found.

`unneeded_include_finder.py -i "../../more_includes" "tugboat.c"`

Analyze "dumptruck.c" and use the ctags executable found at 
"C:/ctags/ctags.exe". 

`unneeded_include_finder.py -c "C:/ctags/ctags.exe" "dumptruck.c"`

Analyze all files in the directory "src"

`unneeded_include_finder.py "src"`

Analyze "scooter.c" and scrub unnecessary includes from the file.

`unneeded_include_finder.py -w "scooter.c"`

Analyze "bicycle.c" and check if "bicycle.h" is not needed. The default 
action is to ignore the complementary headers. 

`unneeded_include_finder.py -m "bicycle.c"`
